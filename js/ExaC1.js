function showCocktails() {
    const alcoholicRadio = document.getElementById("alcoholic");
    const cocktailType = alcoholicRadio.checked ? "Alcoholic" : "Non_Alcoholic";
    const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${cocktailType}`;
  
    fetch(url)
      .then(response => response.json())
      .then(data => {
        const cocktailImages = document.getElementById("cocktail-images");
        cocktailImages.innerHTML = "";
  
        data.drinks.forEach(cocktail => {
          const container = document.createElement("div");

  
          const img = document.createElement("img");
          img.src = cocktail.strDrinkThumb;
          img.alt = cocktail.strDrink;
          container.appendChild(img);
  
          container.appendChild(document.createElement("br")); // Salto de línea
  
          const label = document.createElement("label");
          label.textContent = cocktail.strDrink;
          container.appendChild(label);
  
          cocktailImages.appendChild(container);
        });
  
        document.getElementById("total-cocktails").textContent = data.drinks.length;
      })
      .catch(error => console.error("Error fetching cocktails:", error));
  }
  
  function clearImages() {
    const cocktailImages = document.getElementById("cocktail-images");
    cocktailImages.innerHTML = "";
    document.getElementById("total-cocktails").textContent = "0";
}
